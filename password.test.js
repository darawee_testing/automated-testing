const { checkLength, checkAlphabet,checkSymbol,checkDigit,checkPassword} = require('./password')
describe('Test Password Length', () => {
    test('should 8 characters to be true', () => {
        expect(checkLength('12345678')).toBe(true)
      })
    
      test('should 7 characters to be false', () => {
        expect(checkLength('1234567')).toBe(false)
      })
    
      test('should 25 characters to be true', () => {
        expect(checkLength('1111111111111111111111111')).toBe(true)
      })
    
      test('should 26 characters to be false', () => {
        expect(checkLength('11111111111111111111111111')).toBe(false)
      })  
})

describe('Test Alphabet', () => {
    test('should has alphabet m in password', () => {
      expect(checkAlphabet('m')).toBe(true)
    })
    test('should has alphabet A in password', () => {
      expect(checkAlphabet('A')).toBe(true)
    })
    test('should has not alphabet in password', () => {
      expect(checkAlphabet('1111')).toBe(false)
    })
})

describe('Test Symbol', () => {
    test('should has Symbol ! in password', () => {
      expect(checkSymbol('!11')).toBe(true)
    })
})

describe('Test Digit', () => {
    test('should has Digit 1234 in password', () => {
      expect(checkDigit('1234')).toBe(true)
    })
    test('should has not Digit Amy@yyyyy in password', () => {
      expect(checkDigit('Amy@yyyyy')).toBe(false)
    })
  })

  describe('Test Password', () => {
    test('should Password darawee@1234 in password', () => {
      expect(checkPassword('darawee@1234')).toBe(true)
    })
    test('should Password amy@12 in password', () => {
      expect(checkPassword('amy@12')).toBe(false)
    })
    test('should Password amy12345 in password', () => {
      expect(checkPassword('amy12345')).toBe(false)
    })
    test('should Password amy@yyyyy in password', () => {
      expect(checkPassword('amy@yyyyy')).toBe(false)
    })
  })
  
  
  
  